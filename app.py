#! /usr/bin/python
# -*- coding: utf-8 -*-


from flask import Flask
from flask import render_template, request
import sys, logging

app = Flask(__name__)
app.debug = True
app = Flask(__name__, static_folder='static')

@app.route('/')
def mainpage():
    return render_template('index.html')


@app.route('/ola')
def ola():
    return u"oia"

@app.route('/miau')
def miau():
    return render_template("miau.html")


if __name__ == '__main__':
    from flup.server.fcgi import WSGIServer
    WSGIServer(app).run()
